import {Component, OnInit} from '@angular/core';
import {Plugins} from '@capacitor/core';
import 'contacts';

const {ContactPlugin} = Plugins;

@Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
    contacts = [];

    constructor() {
    }

    async ngOnInit() {
        console.warn('init contacts tab');
    }

    async loadContacts() {
        this.contacts = (await ContactPlugin.getContacts()).result;
        console.log(this.contacts, 'This is a contacts object');
    }

}
